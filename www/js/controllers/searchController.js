'use strict';

angular.module('epaCatalogueInit')

  .controller('searchController', function(searchService,$scope,$stateParams,$state,httpService,$ionicPopup,popUpService,shopService,nlcServices,menuService) {

  $scope.isPhone = shopService.isPhone;
  $scope.date = new Date();
  $scope.searchText = searchService.searchTextFromBack;

  $scope.limit = 10;
  $scope.skip = 0;
  $scope.actualPage = 1;
  $scope.showLeft = false;
  $scope.showRight = false;
  $scope.moreLeft = false;
  $scope.moreRight = false;
  $scope.subPagesArray = [];

  $scope.$watch(function () { return searchService.resultResponse; },
                function (value) {
    if(!$scope.$$phase) {
      $scope.$apply(function(){

        $scope.productList = searchService.resultResponse;
        angular.forEach($scope.productList, function (p) {
          p.price = parseFloat(p.price);
        });
        $scope.productos = searchService.resultSuggestions;
        angular.forEach($scope.productList, function (p) {
          p.price = parseFloat(p.price);
        });
        $scope.getPages(searchService.word);
        $scope.ready = true;
      });
    }else {
      $scope.productList = searchService.resultResponse;
      angular.forEach($scope.productList, function (p) {
        p.price = parseFloat(p.price);
      });
      $scope.productos = searchService.resultSuggestions;
      angular.forEach($scope.productList, function (p) {
        p.price = parseFloat(p.price);
      });
      $scope.getPages(searchService.word);
      $scope.ready = true;



    }
  });
/*
  $scope.prueba = function() {
    searchService.searchCount("wax", 10, function(response) {
      console.log(response);
      searchService.count = response.count;
      console.log(searchService.count);
    });
  };
*/
  //$scope.prueba();

  //Llena los productos encontrados
  //$scope.productList = searchService.resultResponse;
  //Llena los productos sugeridos

  $scope.filterCriteria = [{'name':'Precio mayor','criteria':'price'},{'name':'Precio menor','criteria':'-price'}];
  //$scope.productos = searchService.resultSuggestions;

  //Mostrar y ocultar el simbolo de buscar
  $scope.searching = false;
  $scope.searchOnOff = function(){
    if($scope.searching){
      $scope.searching = false;
    }else {
      $scope.searching = true;
    }
  };
  //Ir a los detalles del productos
  $scope.useProduct= function(element){

    shopService.actualProduct = element;

    $state.go("app.productDetail");
  };



  $scope.updateSubPageArray = function(page) {
    var showNumber;

    if ($scope.isPhone) {
       showNumber = 1;
    }
    else {
      showNumber = 2;
    }


    if ((page - 3) < 1) {
      $scope.subPagesArray = (searchService.pagesArray).slice(0, (page + showNumber));
      //console.log($scope.subPagesArray);
    }
    else {
      $scope.max = (page + showNumber);
      if ($scope.max > searchService.totalPage)
        $scope.max = searchService.totalPage;


      $scope.subPagesArray = (searchService.pagesArray).slice((page - (showNumber + 1)), $scope.max);
      //console.log($scope.subPagesArray);
      //console.log("max :" + $scope.max );
    }

    if ($scope.actualPage == 1)
      $scope.showLeft = false;
    else
      $scope.showLeft = true;

    if ($scope.actualPage == searchService.totalPage)
      $scope.showRight = false;
    else
      $scope.showRight = true;
    //pone los 3 puntitos en donde no se vea el ultimo ni el primer numero

    if ($scope.subPagesArray.indexOf(1) != -1)
        $scope.moreLeft = false;
      else
        $scope.moreLeft = true;


      if ($scope.subPagesArray.indexOf(searchService.totalPage) != -1)
        $scope.moreRight = false;
      else
        $scope.moreRight = true;

  };

  $scope.goToPage = function (page) {
    if (page != $scope.actualPage) {
      //$scope.productRequest(((page - 1) * $scope.limit), $scope.limit);
      $scope.ready=false;
      searchService.search(searchService.word, $scope.limit, ((page - 1) * $scope.limit), function(err, response) {
        searchService.resultResponse = response;
        $scope.actualPage = page;
        $scope.updateSubPageArray(page);
        //console.log("pagina actual " + $scope.actualPage);
        $scope.ready=true;
      });

    }

  };


  $scope.getPages = function(searchWord) {
      if (searchWord) {
          searchService.searchCount(searchWord, 10, function(response) {
            //console.log(response);

            searchService.count = response.count;

            searchService.totalPage = response.numArray[response.numArray.length-1];
            $scope.totalPage2 = searchService.totalPage;

            //console.log("num paginas: "+searchService.totalPage);
            searchService.pagesArray = response.numArray;
            //console.log(searchService.pagesArray);
            //$scope.subPagesArray = searchService.pagesArray;

            //$scope.actualPage = 1;
            $scope.updateSubPageArray($scope.actualPage);
          });
      }

  };



  $scope.nextProducts = function() {
    if ($scope.actualPage >= 1 && $scope.actualPage < searchService.totalPage) {
      $scope.goToPage($scope.actualPage + 1);
      $scope.updateSubPageArray($scope.actualPage);
    }
  };

  $scope.previousProducts = function() {
    if ($scope.actualPage > 1 && $scope.actualPage <= searchService.totalPage) {
      $scope.goToPage($scope.actualPage - 1);
      $scope.updateSubPageArray($scope.actualPage);
    }
  };



  //BuscarProductos
  $scope.searchProductss = function(element){
    if (element != undefined) {
      $scope.ready = false;

      $scope.translateW(element,function(err){





        searchService.search(searchService.word, 10, 0, function(err, response) {

          if (err) {

          }
          else {
            searchService.resultResponse = response;

            if (response.length > 0) {
              //searchService.resultResponse;

              $scope.getPages(searchService.word);
              $scope.actualPage = 1;


              $scope.suggestions(element, function(err) {

                if(!$scope.$$phase) {
                  $scope.$apply(function(){
                    $scope.productos = searchService.resultSuggestions;
                    searchService.searchTextFromBack = element;
                    $scope.ready = true;
                    $state.go('app.search');
                  });
                }else {
                  $scope.productos = searchService.resultSuggestions;
                  searchService.searchTextFromBack = element;
                  $scope.ready = true;
                  $state.go('app.search');
                }
              });
            }
            else {
              popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
              $scope.ready = true;
              //$state.go('app.home');
            }

          }
        });
      });
    }

  };
  //Buscar sugerencias
  $scope.suggestions = function(element,cb){
    searchService.searchSuggest(element,function(err,response){
      if (err) {
        cb(true);
      }
      else {
        searchService.resultSuggestions = response;

        cb(false);

      }
    });
  };
  //Tranducir la palabra buscada
  $scope.translateW = function(element,cb){
    searchService.translate(element,function(err,response){
      if (err) {

        cb(true);
      }
      else {
        searchService.word = response;
        cb(false);
      }
    });
  };



  $scope.data = {
    model: null,
    availableOptions: [
      {id: 'name', name: 'A-Z'},
      {id: '-name', name: 'Z-A'},
      {id: '-price', name: 'Precio(Mayor-Menor)'},
      {id: 'price', name: 'Precio(Menor-Mayor)'}
    ]
  };
  $scope.useSubarea= function(element){
    shopService.actualSubArea = element;
    $state.go('app.productList');
  };

  $scope.useProduct= function(element){
    shopService.actualProduct = element;
    $state.go("app.productDetail");
  };

  $scope.useArea= function(element){
    shopService.actualArea = element;
    $state.go('app.subareasList');
  };
  $scope.menu = function(){
    menuService.getMenuAreas(function(response){
      $scope.areasList=menuService.areasList;
      $scope.ready = true;
    });
  }
  $scope.menu();

});
