angular.module('epaCatalogueInit')

  .controller('productListController', function($scope, $http,$location,$rootScope,
    $sce,$state,httpService,$ionicLoading,$ionicSlideBoxDelegate,$ionicPopup,$stateParams,
    shopService,menuService, UserService,popUpService,searchService, $window) {


  $scope.date = new Date();
  $scope.isPhone = shopService.isPhone;
  $scope.actualUser = UserService.actualUser;
  if (shopService.actualSubArea == undefined) {
    $state.go('app.home');
  };
  $scope.goTo = function(page) {
    $state.go(page);
  };
  $scope.useProduct = function(element) {
    shopService.actualProduct = element;
    $state.go("app.productDetail");
  };
  $scope.useSubarea= function(element){
    console.log(element);
    shopService.actualSubArea = element;
    $state.go('app.productList');
  };
  $scope.useArea= function(element){
    shopService.actualArea = element;
    $state.go('app.subareasList');
  };
  $scope.menu = function(){
    menuService.getMenuAreas(function(response){
      $scope.areasList=menuService.areasList;
      $scope.ready = true;
    });
  };
  $scope.menu();
  $scope.refreshProducts = function(element){
      $scope.ready=false;
      shopService.actualSubArea = element;
      $scope.nameSubarea1 = shopService.actualSubArea.subarea;
      $scope.id = shopService.actualSubArea.id;
      menuService.refreshProducts($scope.id,$scope.nameSubarea1,function(response){
      $scope.productList = response;
      $scope.menu();
      $scope.actualPage = 1;
      $scope.updateSubPageArray(1);
      $scope.ready=false;
      });
  };
  $scope.limit = 10;
  $scope.skip = 0;
  $scope.$watch(function () {
          return shopService.actualSubArea;
      },
      function (value) {
        if (shopService.actualSubArea != undefined) {
              $scope.nameSubarea1 = shopService.actualSubArea.subarea;
              $scope.id = shopService.actualSubArea.id;
              $scope.ready = false;
              $scope.productRequest($scope.skip, $scope.limit);
              $scope.pages();
        };
      }
  );
  $scope.productRequest = function(skip, limit) {
    $scope.ready = false;
    $scope.linkSubarea='/'+$scope.id+'/products?filter[skip]='+skip.toString()+'&filter[limit]='+limit.toString();
    httpService.allHttp('GET', '', 'subareas', $scope.linkSubarea, null, function(response) {
      if (response.error) {
        //popUpService.showPopup('Lo sentimos', 'No se puede obtener la lista de productos en este momento, por favor intente mas tarde');
        $scope.ready = true;
      }
      else {
        $scope.productList = response.response.data;
        $scope.ready = true;
      }
    });
  };
  //$scope.productRequest($scope.skip, $scope.limit);
  $scope.pagesArray = [];
  $scope.subPagesArray = [];
  $scope.actualPage = 1;
  $scope.totalPage;
  $scope.firstPage;
  $scope.lastPage;
  $scope.showLeft = false;
  $scope.showRight = false;
  $scope.moreLeft = false;
  $scope.moreRight = false;
  $scope.pages = function() {
      $scope.linkSubarea='/'+$scope.id+'/products/count';
      httpService.allHttp('GET', '', 'subareas', $scope.linkSubarea, null, function(response) {
        if (response.error) {
          //popUpService.showPopup('Lo sentimos', 'No se puede obtener la lista de productos en este momento, por favor intente mas tarde');
          $scope.ready = true;
        }
        else {
          $scope.data = response.response.data;
          //console.log($scope.data);
          $scope.ready = true;
          //console.log($scope.data.count);
          if ($scope.data.count % $scope.limit != 0){
            $scope.totalPage = parseInt($scope.data.count / $scope.limit);
            $scope.totalPage = $scope.totalPage + 1;
            //console.log($scope.totalPage);
            for(i = 1; i <= $scope.totalPage; i++) {
              $scope.pagesArray.push(i);
            }
            //console.log($scope.pagesArray);
            $scope.updateSubPageArray(1);
          }
          else {
            $scope.totalPage = parseInt($scope.data.count / $scope.limit);
            //console.log($scope.totalPage);
            for(i = 1; i <= $scope.totalPage; i++) {
              $scope.pagesArray.push(i);            }
            //console.log($scope.pagesArray);
            $scope.updateSubPageArray(1);
          }
        }
      });
    };

    //$scope.pages();

    $scope.actualPage = 1;
    $scope.totalPage;

    $scope.updateSubPageArray = function(page) {
      //var showNumber = 2;

      if ($scope.isPhone) {
        showNumber = 1;
      }
      else {
        showNumber = 2;
      }


      if ((page - 3) < 1) {
        $scope.subPagesArray = ($scope.pagesArray).slice(0, (page + showNumber));
        //console.log($scope.subPagesArray);

      }
      else {
        showNumber = 2;
      }

    if ((page - 3) < 1) {
      $scope.subPagesArray = ($scope.pagesArray).slice(0, (page + showNumber));
      //console.log($scope.subPagesArray);
    }
    else {
      $scope.max = (page + showNumber);
      if ($scope.max > $scope.totalPage)
        $scope.max = $scope.totalPage;
      $scope.subPagesArray = ($scope.pagesArray).slice((page - (showNumber + 1)), $scope.max);
      //console.log($scope.subPagesArray);
      //console.log("max :" + $scope.max );
    }
    if ($scope.actualPage == 1)
      $scope.showLeft = false;
    else
      $scope.showLeft = true;
    if ($scope.actualPage == $scope.pagesArray[$scope.pagesArray.length-1])
      $scope.showRight = false;
    else
      $scope.showRight = true;
    //pone los 3 puntitos en donde no se vea el ultimo ni el primer numero
    if ($scope.subPagesArray.indexOf(1) != -1)
      $scope.moreLeft = false;
    else
      $scope.moreLeft = true;
    if ($scope.subPagesArray.indexOf($scope.totalPage) != -1)
      $scope.moreRight = false;
    else
      $scope.moreRight = true;
  };
  $scope.goToPage = function (page) {
    if (page != $scope.actualPage) {
      $scope.productRequest(((page - 1) * $scope.limit), $scope.limit);
      $scope.actualPage = page;
      //console.log("pagina actual " + $scope.actualPage);
      //console.log("y "+$scope.pagesArray);
      $scope.updateSubPageArray(page);
      //console.log("num total de paginas: " + $scope.totalPage);
    }

  };
  $scope.nextProducts = function() {
    if ($scope.actualPage >= 1 && $scope.actualPage < $scope.totalPage) {
      $scope.goToPage($scope.actualPage + 1);
      $scope.updateSubPageArray($scope.actualPage);
    }
  };
  $scope.previousProducts = function() {
    if ($scope.actualPage > 1 && $scope.actualPage <= $scope.totalPage) {
      $scope.goToPage($scope.actualPage - 1);
      $scope.updateSubPageArray($scope.actualPage);
    }
  };
  //BuscarProductos
  $scope.searchProductss = function(element){
    if (element != undefined) {
      $scope.ready = false;

      $scope.translateW(element,function(err){

        searchService.search(searchService.word, 10, 0, function(err,response){
          if (err) {

          }
          else {
            searchService.resultResponse = response;
            if (response.length > 0) {
              searchService.resultResponse;
              $scope.suggestions(element,function(err){

                if(!$scope.$$phase) {
                  $scope.$apply(function(){
                    $scope.productos = searchService.resultSuggestions;
                    searchService.searchTextFromBack = element;
                    $scope.ready = true;
                    $state.go('app.search');
                  });
                }else {
                  $scope.productos = searchService.resultSuggestions;
                  searchService.searchTextFromBack = element;
                  $scope.ready = true;
                  $state.go('app.search');
                }
              });
            }
            else {
              popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
              $scope.ready = true;
              //$state.go('app.home');
            }

          }
        });
      });
    }

  };
  //Tranducir la palabra buscada
  $scope.translateW = function(element,cb){
      searchService.translate(element,function(err,response){
        if (err) {

          cb(true);
        }
        else {
          searchService.word = response;
          cb(false);
        }
      });
    };
  //Buscar sugerencias
  $scope.suggestions = function(element,cb){
    searchService.searchSuggest(element,function(err,response){
        if (err) {

          cb(true);
        }
        else {
          searchService.resultSuggestions = response;

          cb(false);

        }
      });
  };
  $scope.searchOnOff = function(){
    if($scope.searching){
      $scope.searching = false;
    }else {
      $scope.searching = true;
    }
  };

});
