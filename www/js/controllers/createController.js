'use strict';

angular.module('epaCatalogueInit')
.controller('createController',['$stateParams','$state','httpService','$ionicPopup','popUpService','shopService',function($stateParams,$state,httpService,$ionicPopup,popUpService,shopService){
  var user;
  var th = this;
th.isPhone = shopService.isPhone;
th.date = new Date();

 function encrypt(word){
     	return CryptoJS.AES.encrypt(word, "UltraSecret12Password1");
     };

 th.go = function(state){
   $state.go(state);
 };

 th.createUser = function(){

     var objeto = {
         'address':th.address,
         'email':th.email,
         'firstname':th.firstname,
         'lastname':th.lastname,
         'password':(CryptoJS.SHA3(th.password)).toString(CryptoJS.enc.Hex),
         'phone':th.phone,
         'recover':" "
        };

  httpService.allHttp('GET','','users/count/'+th.email,'',null,function(data){
      if (data.error) {
      }
      else{
          user = data.response.data.count;

          if (user == 0) {
              httpService.allHttp('POST','','users','',angular.toJson(objeto),function(response){
                  if (response.error) {

                  }
                  else{
                      popUpService.showPopup("Registro correcto"," "+
                      th.firstname +" su registro ha sido exitoso");
                      $state.go('app.login');
                  }
              });

            }
            else {

                popUpService.showPopup("Correo existente"," "+
                th.email +" ya se encuentra registrado en el sistema");
            }
      }
  });

};

}]);
