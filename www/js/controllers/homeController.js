angular.module('epaCatalogueInit')


  .controller('homeController', function($scope, $http,$location,$rootScope, $sce,$state,httpService,$ionicLoading, $ionicSlideBoxDelegate,$ionicPopup,$stateParams, shopService, popUpService, searchService, $anchorScroll,menuService, UserService) {

  var isAndroid = window.ionic.Platform.isAndroid();
  var isIOS = window.ionic.Platform.isIOS();
  $scope.isPhone = isAndroid || isIOS;
  shopService.isPhone  = $scope.isPhone;
  $scope.actualUser = UserService.actualUser;

  //$scope.actualUser = UserService.actualUser;
  $scope.$watch(
    function () { return UserService.actualUser; },
    function (value) {
      $scope.actualUser = UserService.actualUser;
    }
  );

  $scope.scrollTo = function(target){
    $location.hash(target);   //set the location hash
    //var handle = $ionicScrollDelegate.$getByHandle('myPageDelegate');
    //handle.anchorScroll(true);  // 'true' for animation
  };

  $scope.date = new Date();
  $scope.isPhone = shopService.isPhone;

  $scope.goTo = function(page) {
    $state.go(page);
  };

  $scope.searchOnOff = function(){
    if($scope.searching){
      $scope.searching = false;
    }else {
      $scope.searching = true;
    }
  };
  $scope.menu = function(){

    menuService.getMenuAreas(function(response){
      $scope.areasList=menuService.areasList;

      angular.forEach($scope.areasList , function(value, key) {
        var linkSubarea='/'+value['id'].toString()+'/allProducts/3';
        httpService.allHttp('GET','','areas', linkSubarea, null,function(response){
          if (response.error) {
          }
          else{

            $scope.areasList[key].productsSubList = [];
            angular.forEach(response.response.data.subareas , function(value, key2) {

              angular.forEach(value.products , function(value, key3) {

                $scope.areasList[key].productsSubList.push(value);
                if(key3+1 == response.response.data.subareas.length){

                  $scope.ready = true;
                }
              });
            });
          }
        });
      });
      $scope.ready = true;
    });
  }
  $scope.menu();

  $scope.useArea= function(element){
    shopService.actualArea = element;
    $state.go('app.subareasList');
  };

  $scope.useProduct= function(element){
    shopService.actualProduct = element;
    $state.go("app.productDetail");
  };
  $scope.useSubarea= function(element){

    shopService.actualSubArea = element;
    $state.go('app.productList');
  };

  //BuscarProductos
  $scope.searchProductss = function(element){
    if (element != undefined) {
      $scope.ready = false;
      $scope.translateW(element,function(err){

        searchService.search(searchService.word, 10, 0,function(err,response){
          if (err) {

          }
          else {
            searchService.resultResponse = response;
            if (response.length > 0) {
              searchService.resultResponse;
              $scope.suggestions(element,function(err){

                if(!$scope.$$phase) {
                  $scope.$apply(function(){
                    searchService.resultSuggestions;
                    searchService.searchTextFromBack = element;
                    $scope.ready = true;
                    $state.go('app.search');
                  });
                }
                else {
                  searchService.resultSuggestions;
                  searchService.searchTextFromBack = element;
                  $scope.ready = true;
                  $state.go('app.search');
                }
              });
            }
            else {
              popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
              $scope.ready = true;
              //$state.go('app.home');
            }

          }
        });
      });
    }

  };
  //Buscar sugerencias
  $scope.suggestions = function(element,cb){
    searchService.searchSuggest(element,function(err,response){
      if (err) {
        cb(true);
      }
      else {
        searchService.resultSuggestions = response;
        cb(false);
      }
    });
  };
  //Tranducir la palabra buscada
  $scope.translateW = function(element,cb){
    searchService.translate(element,function(err,response){
      if (err) {
        cb(true);
      }
      else {
        searchService.word = response;
        cb(false);
      }
    });
  };

})

  .controller('menuController', function($scope, $http, $sce,$state,httpService,
    $ionicPopup,$cordovaBarcodeScanner,$cordovaNetwork,shopService,$cordovaToast, popUpService, UserService){
    var isAndroid = window.ionic.Platform.isAndroid();
    var isIOS = window.ionic.Platform.isIOS();
    $scope.isPhone = isAndroid || isIOS;
    shopService.isPhone  = $scope.isPhone;

    $scope.goTo = function(page) {
      if (page === 'app.favorites' || page === 'app.shopCart' || page === 'app.profile' || page === 'app.changePassword') {
        if (UserService.actualUser == undefined) {
          popUpService.showPopup('Inicio de sesión requerido', 'Debe iniciar sesión para la acción indicada');
          UserService.pageToRequiredLogin = page;
          $state.go('app.login');
        }else {
          $state.go(page);
        }
      }else if (page === 'app.promotion') {
        shopService.deal = true;
        $state.go(page);
      }
      else {
        $state.go(page);
      }
    };


    //Método encargado de ejecutar el scaneo del código de barras de un producto
    $scope.scanCode= function(){
      try {
        $cordovaBarcodeScanner
          .scan() //Escaneo del código
          .then(function(barcodeData) {

          $scope.linkProduct='?filter[where][upc]='+barcodeData.text; //Link para el request del producto a la base
          httpService.allHttp('GET','','products',$scope.linkProduct,null,function(response){ //Llama al servicio para el request a la base de datos
            if (response.error) { //Si no se encuentra el producto escaneado se despliega un error
              $cordovaToast.show('No se encontró el producto escaneado', 'short', 'center');

            }else{ //Si se encuentra el producto escaneado, se procede a ver los detalles del mismo
              shopService.actualProduct=response.response.data[0];//Se guardan los datos del producto sacado de la base en una variable para verlos en la vista de detalles dle producto
              //$cordovaToast.show('Producto escaneado encontrado correctamente', 'short', 'center');
              $state.go("app.productDetail");//Se llama a la vista de detalles del producto para ver el producto escaneado
            }
          });
        }, function(error) {
          $cordovaToast.show('No se encontró el producto escaneado', 'short', 'center');
        });

      }catch (e) {
        popUpService.showPopup('Error', 'No se puede realizar esta acción');
      }
    };

    $scope.groups = [

    ];
    $scope.$watch(function () { return shopService.areasList; },
                  function (value) {
      if(!$scope.$$phase) {
        $scope.$apply(function(){
          $scope.groups[0] = {name: 'Areas', id: 1, items: shopService.areasList};
        });
      }else {
        $scope.groups[0] = {name: 'Areas', id: 1, items: shopService.areasList};
      }
    });

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };

    $scope.useArea= function(element){
      shopService.actualArea = element;
      $state.go('app.subareasList');
    };
  });
