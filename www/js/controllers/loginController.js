'use strict';

angular.module('epaCatalogueInit')
.controller('loginController', function($stateParams,$state,httpService,$ionicPopup,
  popUpService, shopService, UserService,$cookies, $scope){
  var th = this;
  th.isPhone = shopService.isPhone;
  th.date = new Date();
  th.Login = [];
  var user;

 th.viewRegistro = 'Registrarse';

 th.go = function(state){
   $state.go(state);
 };

 th.models={
   pass:"password"
 };
 th.viewPassField = function(passFieldName){
   if (th.models[passFieldName]=="text"){
    th.models[passFieldName]="password";
   }else{
     th.models[passFieldName]="text";
   }
 };

th.getUser = function(){
  var password = (CryptoJS.SHA3(th.password)).toString(CryptoJS.enc.Hex);
  var email  =  th.email;

  httpService.allHttp('GET','users','/login','/'+email+'/'+password,null,function(response){
  if (response.error) {

  }
  else{
    if(response.response.data.length == 0){
      popUpService.showPopup('No se ha podido iniciar sesión', 'Correo o contraseña invalidos, por favor verificar los datos de ingreso');

    }else {
        if(!$scope.$$phase) {
            $scope.$apply(function(){
              UserService.actualUser = response.response.data[0];
            });
        }else {
           UserService.actualUser = response.response.data[0];
        }
        var today = new Date();
        var expiresValue = new Date(today);
        expiresValue.setMinutes(today.getMinutes() + 10);
        $cookies.putObject('usr', response.response.data[0],  {'expires' : expiresValue});
        console.log( $cookies.getObject('usr'));
        if(UserService.pageToRequiredLogin != undefined){
            $state.go(UserService.pageToRequiredLogin);
        }else {
          $state.go("app.home");
        }
    }
  }
});
};

});
