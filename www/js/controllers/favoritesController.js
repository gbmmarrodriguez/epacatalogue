'use strict';
angular.module('epaCatalogueInit')
    .controller('favoritesController',
        function ($state, shopService, $ionicModal, httpService, popUpService, UserService, $rootScope, $scope, menuService,searchService) {
            var th = this;
            th.date = new Date();
            th.title = 'My favorite products';
            $scope.isPhone = shopService.isPhone;
            $scope.actualUser = UserService.actualUser;

            $scope.$watch(function () {
                    return UserService.actualUser;
                },
                function (value) {
                    shopService.reloadFavorites(function (favorites) {
                        if (favorites === 1) {
                            UserService.pageToRequiredLogin = 'app.favorites';
                            popUpService.showPopup('Inicio de sesión requerido', 'Debe iniciar sesión para ingresar a favoritos');
                            $state.go('app.login');
                        } else if (favorites === 2) {
                            popUpService.showPopup('Error', 'No se ha podido obtener la información de favoritos');
                            $state.go('app.home');
                        } else {
                            shopService.favoriteList = favorites;
                            th.favoriteList = shopService.favoriteList;
                        }
                    });

                }
            );

            $scope.useProduct = function (element) {
                shopService.actualProduct = element;
                $state.go("app.productDetail");
            };

            th.closeModal = function () {
                th.modal.hide();
            };

            $ionicModal.fromTemplateUrl('templates/favorites/modalQuantity.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                th.modal = modal;
            });

            th.goTo = function (page) {
                $state.go(page);
            };

            th.favoriteList = shopService.favoriteList;
            th.cartList = shopService.cartList;

            $scope.addToCart = function () {

                var quantity = document.getElementById('quantity').value;
                shopService.addToCart(UserService.actualUser.id, th.actualProduct, quantity);
                th.closeModal();
            };

            th.removeFromFavorites = function (index) {
                shopService.removeFromFavorites(UserService.actualUser.id, index);
            };

            th.openModal = function (product) {
                th.actualProduct = product;
                th.modal.show();
            }

            $scope.useSubarea = function (element) {
                shopService.actualSubArea = element;
                $state.go('app.productList');
            };

            $scope.useProduct = function (element) {
                $scope.ready = false;
                shopService.actualProduct = element;
                $state.go("app.productDetail");
                $scope.ready = true;
            };

            $scope.useArea = function (element) {
                shopService.actualArea = element;
                $state.go('app.subareasList');
                $scope.ready = false;
            };
            $scope.menu = function () {
                menuService.getMenuAreas(function (response) {
                    $scope.areasList = menuService.areasList;
                    $scope.ready = true;
                });
            }
            $scope.menu();

            //BuscarProductos
            $scope.searchProductss = function(element){
                if (element != undefined) {
                    $scope.ready = false;

                    $scope.translateW(element,function(err){

                        searchService.search(searchService.word,10,0,function(err,response){
                            if (err) {

                            }
                            else {
                                searchService.resultResponse = response;
                                if (response.length > 0) {
                                    searchService.resultResponse;
                                    $scope.suggestions(element,function(err){

                                        if(!$scope.$$phase) {
                                            $scope.$apply(function(){
                                                $scope.productos = searchService.resultSuggestions;
                                                searchService.searchTextFromBack = element;
                                                $scope.ready = true;
                                                $state.go('app.search');
                                            });
                                        }else {
                                            $scope.productos = searchService.resultSuggestions;
                                            searchService.searchTextFromBack = element;
                                            $scope.ready = true;
                                            $state.go('app.search');
                                        }
                                    });
                                }
                                else {
                                    popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                                    $scope.ready = true;
                                    //$state.go('app.home');
                                }

                            }
                        });
                    });
                }

            };

            //Tranducir la palabra buscada
            $scope.translateW = function(element,cb){
                searchService.translate(element,function(err,response){
                    if (err) {

                        cb(true);
                    }
                    else {
                        searchService.word = response;
                        cb(false);
                    }
                });
            };

            //Buscar sugerencias
            $scope.suggestions = function(element,cb){
                searchService.searchSuggest(element,function(err,response){
                    if (err) {

                        cb(true);
                    }
                    else {
                        searchService.resultSuggestions = response;

                        cb(false);

                    }
                });
            };
            $scope.searchOnOff = function(){
              if($scope.searching){
                $scope.searching = false;
              }else {
                $scope.searching = true;
              }
            };

        });
