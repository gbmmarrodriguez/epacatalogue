angular.module('epaCatalogueInit')

    .controller('promotionController', function ($scope, $http, $location, $rootScope,
        $sce, $state, httpService, $ionicLoading, $ionicSlideBoxDelegate, popUpService,
        $stateParams, shopService, $ionicModal, UserService, menuService, searchService) {

        $scope.actualUser = UserService.actualUser;
        $scope.isPhone = shopService.isPhone;

     if (!shopService.deal) {
              $state.go('app.home');
            }

        $scope.date = new Date();
        $scope.date.setHours(0, 0, 0, 0);
        $scope.valDate = '';
        $ionicModal.fromTemplateUrl('templates/favorites/modalQuantity.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.goTo = function (page) {
            $state.go(page);
        };

        $scope.favoriteList = shopService.favoriteList;
        $scope.linkSubarea = 'deals';

        httpService.allHttp('GET', '', '', $scope.linkSubarea, null, function (response) {
            if (response.error) {} else {
                $scope.productList = response.response.data;
            }
        });

        $scope.search = function () {
            if (Boolean($scope.search.keyword)) {}
        };

        $scope.addToFavorites = function (product, dateP) {
            $scope.useProduct(product);
            $scope.date2 = new Date(dateP);
            if ($scope.date <= $scope.date2) {
                if (UserService.actualUser != undefined) {
                    shopService.addToFavorites(UserService.actualUser.id, shopService.actualProduct);
                } else {
                    popUpService.showPopup('Iniciar sesión', 'Debe iniciar sesión para agregar productos a favoritos');
                    UserService.pageToRequiredLogin = 'app.promotion';
                    $state.go('app.login');
                }
            }
             else {
                popUpService.showPopup('Promoción vencida', 'La promoción ya venció, no se puede agregar a favoritos');
            }
            return $scope.valDate;

        };

        $scope.openModal = function (product,dateP) {
            $scope.date2 = new Date(dateP);
            $scope.useProduct(product);
            if ($scope.date <= $scope.date2) {
                if (UserService.actualUser != undefined) {
                    $scope.actualProduct = product;
                    $scope.modal.show();
                } else {
                    popUpService.showPopup('Iniciar sesión', 'Debe iniciar sesión para agregar productos al carrito');
                    UserService.pageToRequiredLogin = 'app.promotion';
                    $state.go('app.login');
                }
            }

             else {
                popUpService.showPopup('Promoción vencida', 'La promoción ya venció, no se puede agregar al carrito');
            }
            return $scope.valDate;

        };



        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $scope.addToCart = function () {
            var quantity = document.getElementById('quantity').value;
            shopService.addToCart(UserService.actualUser.id, shopService.actualProduct, quantity);
            $scope.closeModal();

        };

        $scope.useProduct = function (element) {
            shopService.actualProduct = element;
        };

        $scope.useProduct2 = function (element) {
            shopService.actualProduct = element;
            $state.go("app.productDetail");
        };

        $scope.data = {
            model: null,
            availableOptions: [
                {
                    id: 'startDate',
                    name: 'Fecha(Actual - Ultima)'
                        },
                {
                    id: '-startDate',
                    name: 'Fecha(Ultima - Actual)'
                        },
                {
                    id: '-price',
                    name: 'Precio(Mayor-Menor)'
                        },
                {
                    id: 'price',
                    name: 'Precio(Menor-Mayor)'
                        }
                     ]
        };
        $scope.data2 = {
            model: null,
            optionsCombo: [
                {
                    id: 'combo',
                    name: 'Combo'
                        },
                {
                    id: 'descuento',
                    name: 'Descuentos'
                        },
                {
                    id: 'mayor',
                    name: 'Al por mayor'
                        },
                {
                    id: 'todas',
                    name: 'Todas'
                        }
                      ],

        };

        //BuscarProductos
        $scope.searchProductss = function(element){
            if (element != undefined) {
                $scope.ready = false;

                $scope.translateW(element,function(err){

                    searchService.search(searchService.word,10,0,function(err,response){
                        if (err) {

                        }
                        else {
                            searchService.resultResponse = response;
                            if (response.length > 0) {
                                searchService.resultResponse;
                                $scope.suggestions(element,function(err){

                                    if(!$scope.$$phase) {
                                        $scope.$apply(function(){
                                            $scope.productos = searchService.resultSuggestions;
                                            searchService.searchTextFromBack = element;
                                            $scope.ready = true;
                                            $state.go('app.search');
                                        });
                                    }else {
                                        $scope.productos = searchService.resultSuggestions;
                                        searchService.searchTextFromBack = element;
                                        $scope.ready = true;
                                        $state.go('app.search');
                                    }
                                });
                            }
                            else {
                                popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                                $scope.ready = true;
                                //$state.go('app.home');
                            }

                        }
                    });
                });
            }

        };

        //Tranducir la palabra buscada
        $scope.translateW = function(element,cb){
            searchService.translate(element,function(err,response){
                if (err) {

                    cb(true);
                }
                else {
                    searchService.word = response;
                    cb(false);
                }
            });
        };

        //Buscar sugerencias
        $scope.suggestions = function(element,cb){
            searchService.searchSuggest(element,function(err,response){
                if (err) {

                    cb(true);
                }
                else {
                    searchService.resultSuggestions = response;

                    cb(false);

                }
            });
        };
        $scope.searchOnOff = function(){
          if($scope.searching){
            $scope.searching = false;
          }else {
            $scope.searching = true;
          }
        };
    });
