'use strict';

angular.module('epaCatalogueInit')
.controller('recoverController',function($stateParams,$state,httpService,
  popUpService,$http,shopService, mailService){
    var th = this;
    th.date = new Date();
    th.isPhone = shopService.isPhone;
    th.viewRestore="Restablecer contraseña";
    th.restore = function(){
    $stateParams.restoreCode;
//primero obtener con la peticion el restoreCode
        var objeto = {
        'recover':makeRandomRecoverCode(),
        'password':(CryptoJS.SHA3(th.password)).toString(CryptoJS.enc.Hex)
        };

         httpService.allHttp('POST','','users/recovery/recovercode/'+$stateParams.restoreCode,'',angular.toJson(objeto),function(response){

             if (response.error) {
             }
             else{
               response=response.response.data;
               if (response.count==0){;
                 popUpService.showPopup("Error","La dirección a la que ingresó"+
                 " ya expiró o es incorrecta");
               }
               else{
                   popUpService.showPopupGo("Cambio exitoso","Se ha realizado el cambio de contraseña"+
                   " de manera correcta.",'app.login');
                   th.cpassword="";
                   th.password="";
               }
           }

         });


    };



    th.go = function(state){
        $state.go(state);
      };

      function makeRandomRecoverCode(){
        var response = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 12; i++ )
            response += possible.charAt(Math.floor(Math.random() * possible.length));
        return response;
      }

      th.emailExists = function(){
   var objeto = {
     'email':th.email
   };

   httpService.allHttp('GET','','users/count/'+objeto.email,'',null,function(response){
     if (response.error) {
     }
     else{
       response=response.response.data;

       if (response.count==0){

         popUpService.showPopup("Correo no existente","El correo que ingresó"+
         " no se encuentra en nuestro sistema");
       }else{
         var recoverCode=makeRandomRecoverCode();
         var text=" Favor ingresar a la siguiente dirección para restablecer su contraseña: "+
         "https://sitioepacatalogo.mybluemix.net/#/app/restore/"+recoverCode;


         //Postear el recoverCode al collection con el respectivo email
         var objeto={
           recover:recoverCode
         };
         httpService.allHttp('POST','','users/recovery/email/'+th.email,'',angular.toJson(objeto),function(response){
             if (response.error) {
             }
             else{
                 //Enviar correo aqui
                 var objeto2={
                   toEmail:th.email,
                   subject:"Recuperación de Contraseña EPA Catalog",
                   content:text
                 };
                 mailService.sendMail('POST','','sendMail/','',angular.toJson(objeto2),function(response){
                     if (response.error) {
                     }
                     else{
                         response=response.response.data.statusCode;
                         if(response=="Ok"){
                           popUpService.showPopupGo("Correo enviado",
                           "En su correo encontrará instrucciones para "+
                         "restablecer su contraseña",'app.login');
                         }else{
                           popUpService.showPopup("Error","No se ha enviado "+
                           "su correo, favor ponerse en contacto con EPA");
                         }
                     }
                 });

             }
         });


       }//Fin else count==0
     }
   });//Fin primera pet
 }

});
