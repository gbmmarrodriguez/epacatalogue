angular.module('epaCatalogueInit')

.controller('productDetailController',
function(searchService, $scope, $http,$location,$rootScope, $sce,$state, httpService,
  $ionicLoading,$ionicSlideBoxDelegate,popUpService,$stateParams, shopService,$ionicModal,
  UserService,menuService){
    $scope.date = new Date();
    $scope.isPhone = shopService.isPhone;
    $scope.actualUser = UserService.actualUser;

    if (shopService.actualProduct == undefined) {
        $state.go('app.home');
      };
        $ionicModal.fromTemplateUrl('templates/favorites/modalQuantity.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.goTo = function(page) {
            $state.go(page);
        };

        $scope.$watch(function () {
                return shopService.actualProduct;
            },
            function (value) {
              if (shopService.actualProduct != undefined) {
                $scope.favoriteList = shopService.favoriteList;
                $scope.productName= shopService.actualProduct.name;
                $scope.productId= shopService.actualProduct.id || shopService.actualProduct._id ;
                $scope.linkProduct='/'+$scope.productId;
                httpService.allHttp('GET','','products',$scope.linkProduct,null,function(response){
                    if (response.error) {
                        $scope.ready = true;
                    }else{
                        $scope.productDetail = response.response.data;
                        $scope.ready = true;
                    }
                });
              };
            }
        );

        $scope.search=function(){
            if(Boolean($scope.search.keyword)){
                //window.location="#/app/searchresult/"+$scope.search.keyword;
            }
        };

        $scope.addToFavorites = function(){
            if(UserService.actualUser != undefined){
                shopService.addToFavorites(UserService.actualUser.id, shopService.actualProduct);
            }else {
                popUpService.showPopup('Iniciar sesión','Debe iniciar sesión para agregar productos a favoritos');
                UserService.pageToRequiredLogin = 'app.productDetail';
                $state.go('app.login');
            }
        };

        $scope.openModal = function(product){
            if(UserService.actualUser != undefined){
                $scope.actualProduct = product;
                $scope.modal.show();
            }else {
                popUpService.showPopup('Iniciar sesión','Debe iniciar sesión para agregar productos al carrito');
                UserService.pageToRequiredLogin = 'app.productDetail';
                $state.go('app.login');
            }
        };

        $scope.closeModal = function(){
            $scope.modal.hide();
        };

        $scope.addToCart = function(){
            var quantity = document.getElementById('quantity').value;
            shopService.addToCart(UserService.actualUser.id, shopService.actualProduct, quantity);
            $scope.closeModal();

        };

        $scope.searching = false;
        $scope.searchOnOff = function(){
            if($scope.searching){
                $scope.searching = false;
            }else {
                $scope.searching = true;
            }
        };

        //BuscarProductos
        $scope.searchProductss = function(element){
            if (element != undefined) {
                $scope.ready = false;

                $scope.translateW(element,function(err){

                    searchService.search(searchService.word,10, 0, function(err,response){
                        if (err) {

                        }
                        else {
                            searchService.resultResponse = response;
                            if (response.length > 0) {
                                searchService.resultResponse;
                                $scope.suggestions(element,function(err){

                                    if(!$scope.$$phase) {
                                        $scope.$apply(function(){
                                            $scope.productos = searchService.resultSuggestions;
                                            searchService.searchTextFromBack = element;
                                            $scope.ready = true;
                                            $state.go('app.search');
                                        });
                                    }else {
                                        $scope.productos = searchService.resultSuggestions;
                                        searchService.searchTextFromBack = element;
                                        $scope.ready = true;
                                        $state.go('app.search');
                                    }
                                });
                            }
                            else {
                                popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                                $scope.ready = true;
                                //$state.go('app.home');
                            }

                        }
                    });
                });
            }

        };
        $scope.useSubarea= function(element){
            shopService.actualSubArea = element;
            $state.go('app.productList');
        };

        $scope.useProduct= function(element){
            shopService.actualProduct = element;
            $state.go("app.productDetail");
        };

        $scope.useArea= function(element){
               shopService.actualArea = element;
               $state.go('app.subareasList');
           };
        $scope.menu = function(){
            menuService.getMenuAreas(function(response){
                $scope.areasList=menuService.areasList;
                $scope.ready = true;
            });
        }
        $scope.menu();


        //Tranducir la palabra buscada
        $scope.translateW = function(element,cb){
            searchService.translate(element,function(err,response){
                if (err) {

                    cb(true);
                }
                else {
                    searchService.word = response;
                    cb(false);
                }
            });
        };

        //Buscar sugerencias
        $scope.suggestions = function(element,cb){
            searchService.searchSuggest(element,function(err,response){
                if (err) {

                    cb(true);
                }
                else {
                    searchService.resultSuggestions = response;

                    cb(false);

                }
            });
        };
        $scope.searchOnOff = function(){
          if($scope.searching){
            $scope.searching = false;
          }else {
            $scope.searching = true;
          }
        };

});
