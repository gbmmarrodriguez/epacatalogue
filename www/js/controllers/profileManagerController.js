'use strict';

angular.module('epaCatalogueInit')

.controller('profileManagerController', function($scope, $state, shopService,
  httpService, UserService, popUpService, $http, mailService,menuService,searchService){

      $scope.isPhone = shopService.isPhone;

      $scope.$watch(function () {
      return UserService.actualUser; },
      function (value) {
            if (value==undefined){
              $state.go('app.login');
            }else{
              $scope.actualUser=value;
              $scope.profile.address=value.address;
              $scope.profile.lastname=value.lastname;
              $scope.profile.firstname=value.firstname;
              $scope.profile.phone=parseInt(value.phone);
            }
      }
    );

    $scope.go = function(state){
      $state.go(state);
    };

    $scope.modifyUser=function(){
      console.log(1);
      var value=UserService.actualUser;
      var objeto={
        address:$scope.profile.address,
        lastname:$scope.profile.lastname,
        firstname:$scope.profile.firstname,
        phone:$scope.profile.phone
      };
      httpService.allHttp('POST','','users/recovery/email/'+UserService.actualUser.email,'',angular.toJson(objeto),function(response){
          if (response.error) {
              //console.error(response);
          }
          else{
              console.log("Usuario Modificado");
              UserService.actualUser.address=objeto.address;
              UserService.actualUser.lastname=objeto.lastname;
              UserService.actualUser.firstname=objeto.firstname;
              UserService.actualUser.phone=objeto.phone;
              popUpService.showPopup("Usuario Modificado","Se han modificado "+
              "correctamente los datos");
          }
      });//Fin del httpService
    };//Fin de modifyUser
    //BuscarProductos
    $scope.searchProductss = function(element){
        if (element != undefined) {
            $scope.ready = false;

            $scope.translateW(element,function(err){

                searchService.search(searchService.word,function(err,response){
                    if (err) {

                    }
                    else {
                        searchService.resultResponse = response;
                        if (response.length > 0) {
                            searchService.resultResponse;
                            $scope.suggestions(element,function(err){

                                if(!$scope.$$phase) {
                                    $scope.$apply(function(){
                                        $scope.productos = searchService.resultSuggestions;
                                        searchService.searchTextFromBack = element;
                                        $scope.ready = true;
                                        $state.go('app.search');
                                    });
                                }else {
                                    $scope.productos = searchService.resultSuggestions;
                                    searchService.searchTextFromBack = element;
                                    $scope.ready = true;
                                    $state.go('app.search');
                                }
                            });
                        }
                        else {
                            popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                            $scope.ready = true;
                            //$state.go('app.home');
                        }

                    }
                });
            });
        }

    };

    //Tranducir la palabra buscada
    $scope.translateW = function(element,cb){
        searchService.translate(element,function(err,response){
            if (err) {

                cb(true);
            }
            else {
                searchService.word = response;
                cb(false);
            }
        });
    };

    //Buscar sugerencias
    $scope.suggestions = function(element,cb){
        searchService.searchSuggest(element,function(err,response){
            if (err) {

                cb(true);
            }
            else {
                searchService.resultSuggestions = response;

                cb(false);

            }
        });
    };
});
