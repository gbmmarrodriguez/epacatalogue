angular.module('epaCatalogueInit')

.controller('subareasListController', function( $scope, $http,$location,
  $rootScope, $sce,$state,httpService,$ionicLoading, $ionicSlideBoxDelegate,
  $ionicPopup,$stateParams, shopService, searchService, $anchorScroll,
  menuService, UserService, popUpService){

      $scope.actualUser = UserService.actualUser;
        $scope.scrollTo = function(target){
            $location.hash(target);   //set the location hash
            //var handle = $ionicScrollDelegate.$getByHandle('myPageDelegate');
            //handle.anchorScroll(true);  // 'true' for animation
        };

        $scope.date = new Date();
        $scope.isPhone = shopService.isPhone;
        if (shopService.actualArea == undefined) {
            $state.go('app.home');
        }else {

            $scope.goTo = function(page) {
                $state.go(page);
            };

            $scope.searchOnOff = function(){
                if($scope.searching){
                    $scope.searching = false;
                }else {
                    $scope.searching = true;
                }
            };

            $scope.nameArea1= shopService.actualArea.area;
            $scope.id= shopService.actualArea.id;
            $scope.linkArea='/'+$scope.id+'/subareas';

            httpService.allHttp('GET','','areas',$scope.linkArea, null,function(response){

                if (response.error) {
                    //console.error(response);
                    popUpService.showPopup('Lo sentimos', 'No se pueden obtener las subareas en este momento, por favor intente mas tarde');
                    $scope.ready = true;
                }
                else{
                    $scope.subareasList = response.response.data;
                    angular.forEach($scope.subareasList, function(value, key) {
                        //console.log(value['id'].toString());
                        var linkSubarea='/'+value['id'].toString()+'/products?filter[limit]=8';
                        //console.log(linkSubarea);
                        httpService.allHttp('GET','','subareas', linkSubarea, null,function(response){
                            if (response.error) {
                            }
                            else{
                                //console.log(response.response.data);
                                //value['productsSubList'] = response.response.data;
                                $scope.subareasList[key].productsSubList = response.response.data;
                                if(key+1 == $scope.subareasList.length){
                                    $scope.ready = true;
                                }
                            }
                        });
                    });
                }
            });


            $scope.useSubarea= function(element){
                shopService.actualSubArea = element;
                $state.go('app.productList');
            };

            $scope.useProduct= function(element){
                shopService.actualProduct = element;
                $state.go("app.productDetail");
            };

            $scope.refreshSubAreas = function(element){
                $scope.ready=false;
                shopService.actualArea = element;
                $scope.nameArea1= shopService.actualArea.area;
                $scope.id= shopService.actualArea.id;

                menuService.refreshSubAreas($scope.nameArea1,$scope.id,function(response){
                    $scope.subareasList = response;
                    angular.forEach(response, function(value, key) {
                        //console.log(value['id'].toString());
                        var linkSubarea='/'+value['id'].toString()+'/products?filter[limit]=8';
                        //console.log(linkSubarea);
                        httpService.allHttp('GET','','subareas', linkSubarea, null,function(response){
                            if (response.error) {
                            }
                            else{
                                $scope.subareasList[key].productsSubList = response.response.data;
                                if(key+1 == $scope.subareasList.length){
                                    $scope.ready = true;
                                }
                            }
                        });
                    });
                });
            }

            $scope.menu = function(){
                menuService.getMenuAreas(function(response){
                    $scope.areasList=menuService.areasList;
                    $scope.ready = true;
                });
            }
            $scope.menu();


            /**/

            $scope.searching = false;
            $scope.searchOnOff = function(){
                if($scope.searching){
                    $scope.searching = false;
                }else {
                    $scope.searching = true;
                }
            };

            //BuscarProductos
            $scope.searchProductss = function(element){
                if (element != undefined) {
                    $scope.ready = false;

                    $scope.translateW(element,function(err){

                        searchService.search(searchService.word, 10, 0, function(err,response){
                            if (err) {

                            }
                            else {
                                searchService.resultResponse = response;
                                if (response.length > 0) {
                                    searchService.resultResponse;
                                    $scope.suggestions(element,function(err){

                                        if(!$scope.$$phase) {
                                            $scope.$apply(function(){
                                                $scope.productos = searchService.resultSuggestions;
                                                searchService.searchTextFromBack = element;
                                                $scope.ready = true;
                                                $state.go('app.search');
                                            });
                                        }else {
                                            $scope.productos = searchService.resultSuggestions;
                                            searchService.searchTextFromBack = element;
                                            $scope.ready = true;
                                            $state.go('app.search');
                                        }
                                    });
                                }
                                else {
                                    popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                                    $scope.ready = true;
                                    //$state.go('app.home');
                                }

                            }
                        });
                    });
                }

            };

            //Tranducir la palabra buscada
            $scope.translateW = function(element,cb){
                searchService.translate(element,function(err,response){
                    if (err) {

                        cb(true);
                    }
                    else {
                        searchService.word = response;
                        cb(false);
                    }
                });
            };

            //Buscar sugerencias
            $scope.suggestions = function(element,cb){
                searchService.searchSuggest(element,function(err,response){
                    if (err) {

                        cb(true);
                    }
                    else {
                        searchService.resultSuggestions = response;

                        cb(false);

                    }
                });
            };
        }
    });
