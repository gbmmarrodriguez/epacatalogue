'use strict';

angular.module('epaCatalogueInit')
.controller('modifyPassController', function($scope, $state, shopService,
  httpService, UserService, popUpService, $http, mailService){

$scope.actualUser = UserService.actualUser;
      $scope.isPhone = shopService.isPhone;

      $scope.$watch(function () { return UserService.actualUser; },
      function (value) {
            if (value==undefined){
              $state.go('app.login');
            }else{
              $scope.actualUser=value
            }
      }
    );

    function encrypt(word){
        	return CryptoJS.AES.encrypt(word, "UltraSecret12Password1");
    };

    $scope.go = function(state){
      $state.go(state);
    };
    $scope.models={
      passOld:"password",
      passNew:"password",
      passConfirm:"password"
    };
    $scope.viewPassField = function(passFieldName){
      if ($scope.models[passFieldName]=="text"){
        $scope.models[passFieldName]="password";
      }else{
        $scope.models[passFieldName]="text";
      }
    };

    $scope.changePassword=function(){
      var value=UserService.actualUser;
      var actualPass=value.password;
      var verifyPass=$scope.modifyPass.passOld;
      var encriptedSupposedPass=(CryptoJS.SHA3(verifyPass)).toString(CryptoJS.enc.Hex);
      var newPass=$scope.modifyPass.passNew;
      var encriptedNewPass=(CryptoJS.SHA3(newPass)).toString(CryptoJS.enc.Hex);
      if (actualPass==encriptedSupposedPass){
        var objeto={
          password:encriptedNewPass
        };
        httpService.allHttp('POST','','users/recovery/email/'+UserService.actualUser.email,'',angular.toJson(objeto),function(response){
            if (response.error) {
                //console.error(response);
            }
            else{
                UserService.actualUser.password=objeto.password;
                popUpService.showPopup("Contraseña Modificada","Se ha modificado "+
                "correctamente su contraseña");
            }
        });//Fin del httpService

      }else{
        popUpService.showPopup("Usuario No Modificado","Su clave anterior es incorrecta ");
      }
    };//Fin de modifyPass


});
