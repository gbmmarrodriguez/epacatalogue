'use strict';

angular.module('epaCatalogueInit')
.controller('shopCartController', function($scope, $state, shopService,
  httpService, UserService, popUpService, ordersService, $http, mailService,
  menuService,searchService){

      $scope.actualUser = UserService.actualUser;
      $scope.isPhone = shopService.isPhone;
      $scope.cartList = [];
      var th = this;
      $scope.getTotal = function(array){
          var total = 0;
            for(var i = 0; i < array.length; i++){
              total += (array[i].product.price * array[i].cartquantity);
          }
          return total ;
      };

  $scope.$watch(function () { return UserService.actualUser; },
  function (value) {
    shopService.reloadCarts(function(carts){
      if (carts === 1){
        popUpService.showPopup('Inicio de sesión requerido', 'Debe iniciar sesión para ingresar al carrito de compra');
        $state.go('app.login');
      }else if (carts === 2) {
        popUpService.showPopup('Error', 'No se ha podido obtener la información del carrito de compra');
        $state.go('app.home');
      }else {
        shopService.cartList = carts;
        $scope.cartList = shopService.cartList;
      }
    });

  }
);

$scope.goTo = function(page) {
  $state.go(page);
};

 $scope.useProduct = function (element) {
     shopService.actualProduct = element;
     $state.go("app.productDetail");
 };

$scope.cartList = shopService.cartList;

$scope.removeFromCart = function(index){
  shopService.removeFromCart(UserService.actualUser.id,index);
};

$scope.buy = function(){

  popUpService.askHeadquarterClient($scope, function(headquarter){
    if(headquarter != undefined){
      UserService.actualUser.phone;
      console.log(UserService.actualUser);
      var request = ordersService.GETApi('orders?filter[where][client_id]='+ UserService.actualUser.phone.trim()+'&filter[where][status]='+'1');
      request.then(function(result){
        // Si el resultado es mayor a 0 es porque el cliente tiene alguna comanda registrada en caso contrario tiene que crear
        // una nueva comanda
        console.log(result);
        if(result.length > 0){
          popUpService.showPopup('¡Aviso! No se puede realizar la orden', 'En este momento existe una orden activa');
        }else{
            var order = {'clientId':UserService.actualUser.phone.trim(), 'headquarter':headquarter}
            newOrder(order);
        }
    });
  }
  });
};

function newOrder(order){
  var request = ordersService.POSTAddOrder(order.clientId, order.headquarter, 'usuario_catalogo');
  request.then(function(result){

    var productsArray = generateJsonArray();
    var request = ordersService.POSTAddProductoTOOrder(result.order.order_id, 10152, 'usuario_catalogo', productsArray);
    request.then(function(result){


      /******************/
      var carProducts=shopService.cartList;
      var email=UserService.actualUser.email;
      mailService.sendBillMail('POST','','sendMail/','',carProducts,email,function(response){

        if (response.error){
          popUpService.showPopup("Error","Favor ponerse en contacto con EPA");
        }
        else{
          response=response.response.data.statusCode;
          if(response=="Ok"){
            popUpService.showPopup('¡Transacción confirmada!', 'Se ha creado correctamente la reserva, se enviará a su correo la orden');
            shopService.cartList = [];
          }else{
            popUpService.showPopup("Error","Favor ponerse en contacto con EPA");
          }
        }
      });//Fin de envio de correo

      /******************/
    }).catch(function(error){

    //  console.error('No se pudieron agregar los productos a la comanda');
    });


  }).catch(function(error){
    popUpService.showPopup('¡Error!', 'No se ha podido crear la reserva');
  });
};

function generateJsonArray() {
  var productsArray = [];
  var jsonFile = {};
  angular.forEach(shopService.cartList, function(value, key){
//    console.log("value: ", value);
    jsonFile = "{\"product_id\":\"" + String(value.product.upc) + "\", \"quantity\":" + value.cartquantity+", \"discount\":"
    + "0" + ", \"reason\":\"" + "" + "\"" +"}";
    productsArray.push(JSON.parse(jsonFile));
  });

  return productsArray;
};

$scope.changeQuantity= function(action, product){
  if(action === 'less'){
    if(product.cartquantity >1){
      product.cartquantity--;
    }
  }else {
    product.cartquantity++;
  }

  shopService.updateQuantity(UserService.actualUser.id, $scope.cartList);
};
$scope.useSubarea= function(element){
    shopService.actualSubArea = element;
    $state.go('app.productList');
};

$scope.useProduct= function(element){
    shopService.actualProduct = element;
    $state.go("app.productDetail");
};

$scope.useArea= function(element){
       shopService.actualArea = element;
       $state.go('app.subareasList');
   };
$scope.menu = function(){
    menuService.getMenuAreas(function(response){
        $scope.areasList=menuService.areasList;
        $scope.ready = true;
    });
}
$scope.menu();
//BuscarProductos
$scope.searchProductss = function(element){
    if (element != undefined) {
        $scope.ready = false;

        $scope.translateW(element,function(err){

            searchService.search(searchService.word,10,0,function(err,response){
                if (err) {

                }
                else {
                    searchService.resultResponse = response;
                    if (response.length > 0) {
                        searchService.resultResponse;
                        $scope.suggestions(element,function(err){

                            if(!$scope.$$phase) {
                                $scope.$apply(function(){
                                    $scope.productos = searchService.resultSuggestions;
                                    searchService.searchTextFromBack = element;
                                    $scope.ready = true;
                                    $state.go('app.search');
                                });
                            }else {
                                $scope.productos = searchService.resultSuggestions;
                                searchService.searchTextFromBack = element;
                                $scope.ready = true;
                                $state.go('app.search');
                            }
                        });
                    }
                    else {
                        popUpService.showPopup('Sin resultado','No se encontraron productos relacionados con '+ element);
                        $scope.ready = true;
                        //$state.go('app.home');
                    }

                }
            });
        });
    }

};

//Tranducir la palabra buscada
$scope.translateW = function(element,cb){
    searchService.translate(element,function(err,response){
        if (err) {

            cb(true);
        }
        else {
            searchService.word = response;
            cb(false);
        }
    });
};

//Buscar sugerencias
$scope.suggestions = function(element,cb){
    searchService.searchSuggest(element,function(err,response){
        if (err) {

            cb(true);
        }
        else {
            searchService.resultSuggestions = response;

            cb(false);

        }
    });
};
$scope.searchOnOff = function(){
  if($scope.searching){
    $scope.searching = false;
  }else {
    $scope.searching = true;
  }
};

});
