'use strict';

angular.module('epaCatalogueInit')
.controller('shoppingHistoryController',['$scope','$state', function($scope, $state){
    $scope.title = 'My shopping history';
    $scope.date = new Date();
    $scope.isPhone = shopService.isPhone;

    $scope.goTo = function(page) {
        $state.go(page);
    };

}]);
