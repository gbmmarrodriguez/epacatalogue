
angular.module('epaCatalogueInit', ['ionic','ionic.cloud', 'ngCordova', 'angularUtils.directives.dirPagination', 'LocalStorageModule','angularMoment','aCarousel','ngCookies'])

  .run(function($ionicPlatform, $state, globalService, $rootScope, UserService) {

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });


})


  .config(function($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu/menu.html',
    controller: 'menuController'
  })
    .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/home/home.html",
        controller: 'homeController'
      }
    },
    authentication : false,
    params: {
      'actualUser': {}
    }
  })
    .state('app.subareasList', {
    url: "/subareasList",
    views: {
      'menuContent': {
        templateUrl: "templates/subarea/subareasList.html",
        controller: 'subareasListController'
      }
    },
    authentication : false
  })
    .state('app.productList', {
    url: "/productList",
    views: {
      'menuContent': {
        templateUrl: "templates/product/productList.html",
        controller: 'productListController'
      }
    },
    authentication : false
  })
    .state('app.productDetail', {
    url: "/productDetail",
    views: {
      'menuContent': {
        templateUrl: "templates/product/productDetail.html",
        controller: 'productDetailController'
      }
    },
    authentication : false
  })
    .state('app.favorites', {
    url: "/favorites",
    views: {
      'menuContent': {
        templateUrl: "templates/favorites/favorites.html",
        controller: 'favoritesController',
        controllerAs:'favorites'
      }
    },
    authentication : true
  })
    .state('app.shopCart', {
    url: "/shopCart",
    views: {
      'menuContent': {
        templateUrl: "templates/shopCart/shopCart.html",
        controller: 'shopCartController'
      }
    },
    authentication : true
  })
    .state('app.shoppingHistory', {
    url: "/shoppingHistory",
    views: {
      'menuContent': {
        templateUrl: "templates/shoppingHistory/shoppingHistory.html",
        controller: 'shoppingHistoryController'
      }
    },
    authentication : true
  })
    .state('app.login',{
    url:'/login',
    views: {
      'menuContent': {
        templateUrl:'templates/loginModule/Login.html',
        controller:'loginController',
        controllerAs:'login'
      }
    },
    authentication : false
  })
    .state('app.profile',{
    url:'/profile',
    views: {
      'menuContent': {
        templateUrl:'templates/profileManagerModule/profile.html',
        controller:'profileManagerController',
        controllerAs:'profile'
      }
    },
    authentication : false
  })
    .state('app.changePassword',{
    url:'/changePassword',
    views: {
      'menuContent': {
        templateUrl:'templates/passwordProfile/changePass.html',
        controller:'modifyPassController',
        controllerAs:'modifyPass'
      }
    },
    authentication : false
  })
    .state('app.create',{
    url:'/create',
    views: {
      'menuContent': {
        templateUrl:'templates/loginModule/createUser.html',
        controller:'createController',
        controllerAs:'create'
      }
    },
    authentication : false
  })
    .state('app.restore',{
    url:'/restore/:restoreCode',
    views: {
      'menuContent': {
        templateUrl:'templates/recoverPasswordModule/restore.html',
        controller:'recoverController',
        controllerAs:'restore'
      }
    },
    authentication : false
  })
    .state('app.recover',{
    url:'/recover',
    views: {
      'menuContent': {
        templateUrl:'templates/recoverPasswordModule/recover.html',
        controller:'recoverController',
        controllerAs:'recover'
      }
    },
    authentication : false
  })
    .state('app.search',{
    url:'/search',
    views: {
      'menuContent': {
        templateUrl:'templates/search/search.html',
        controller:'searchController',
        controllerAs:'search'
      }
    },
    authentication : false
  })
    .state('app.promotion', {
    url: "/promotion",
    views: {
      'menuContent': {
        templateUrl: "templates/promotion/promotion.html",
        controller: 'promotionController'
      }
    },
    authentication : false
  })
  ;

  $urlRouterProvider.otherwise('/app/home');
});
