'use strict';

angular.module('epaCatalogueInit')
  .service('searchService', function(httpService, popUpService,nlcServices,UserService,translateService){

  var th = this;

  th.translate = function(keyword,cb){

    translateService.allHttp('GET',keyword+'/es-en',function(response){
      if (response.error) {
        cb(true,response);
      }
      else {
        cb(false,response.message.data);
      }
    });
  };

  th.searchCount = function(keyword, limit, cb){
    var object = {
      "criteria": keyword.toString() || '',
      "limit": limit
    };

    httpService.allHttp('POST', 'products', '/searchcount', '', angular.toJson(object), function(response){
      if (response.error) {
        cb(true, response);
      }
      else {
        cb(response.response.data);
        //console.log(response.response.data.count);

      }
    });
  };


  th.search = function(keyword, limit, skip, cb){

    var object = {
      "criteria":keyword.toString(),
      "limit": limit,
      "skip": skip
    };

    httpService.allHttp('POST','products','/search','',angular.toJson(object), function(response){
      if (response.error) {
        cb(true,response);
      }
      else {
        cb(false,response.response.data);
      }
    });

  };




  //Search start
  th.searchSuggest = function(keyword,cb){
    var add=0;
    var avg;
    var cont=0;
    var confidence = 0;
    var arr;
    var subName = new Array();
    var arrIdSub = new Array();
    var products = new Array();
    var showProducts = new Array();
    var idSubArea;
    var subArea;
    var array;
    var finalProducts ;
    products.lenght = 0;
    //Limpiar para arreglos nuevos
    //Recomendaciones de watson por sub-areas
    nlcServices.allHttp('GET',keyword.toString(), function(response){
      if(response.error){
        //console.log({'error':true, 'message':response});
      }
      else {
        array = response.message.data.classes;
        for (var i = 0; i < array.length; i++) {
          confidence = array[i].confidence;
          arr = array[i];
          cont = i + 1;
          add = add + parseFloat(confidence);
        }
        avg = (add / cont) + 0.01;
        console.log(avg);
        for (var j = 0; j < array.length; j++) {
          if (parseFloat(array[j].confidence) > avg ) {
            subName.push(array[j].class_name);//Subareas que recomienda watson
          }
        }
      };
      //Obtener todas las subareas
      httpService.allHttp('GET','','','subareas', null,function(response){
        if (response.error) {
          //    console.error(response);
        }
        else{
          array = response.response.data;//Todas las subareas
          for (var i = 0; i < subName.length; i++) {//recorre el arreglo de watson
            for (var j = 0; j < array.length; j++) {//recorre el arreglo de las subareas
              subArea = array[j].subarea.replace(/&amp;/g, "&").replace(/,/g, " &");// se parsean para que sean iguales
              if (subArea == subName[i]) {//compara si son iguales
                arrIdSub.push(array[j].id);//guarda en el arreglo las iguales
              }
            }
          }
          for (var i = 0; i < arrIdSub.length; i++) {//recorrer el arreglo con las subareas iguales
            idSubArea = arrIdSub[i];//Cada subarea
            //Obtener los productos de la subareas
            httpService.allHttp('GET','','subareas','/'+idSubArea +'/products', null,function(response){
              if (response.error) {
                //   console.error(response);
              }
              else{
                var productss = response.response.data; //Se trae los productos de todas las subareas sugeridas
                for (var i = 0; i < productss.length; i++){//Recorre todos los productos de las subareas
                  products.push(productss[i]);//Guarda todos los productos de las subareas sugeridas
                }
                for (var i = 0; i < 5; i++) {
                  var randomProducts = products[Math.floor(Math.random() * products.length)];
                  showProducts.push(randomProducts);
                  console.log(randomProducts);
                  i+=1;
                  //recorre dos veces cada una de las subareas para extraer dos productos random
                }
                cb(false,showProducts);
              }
            });
          }
        }

      });
    });
  };
  //Search end

});
