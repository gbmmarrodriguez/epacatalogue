'use strict';

angular.module('epaCatalogueInit')
.service('nlcServices',function($http){

var service = {};


var urlBase='https://testnlcwatsonESEN.mybluemix.net/';

service.allHttp = function(httpMethod,param,callback){
var req ={
    async : true,
    crossDomain : true,
    method: httpMethod,
    headers :{
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
    }
};

req.url = urlBase + param;


return $http(req).then(function(response){
    callback({'error': false, 'message':response});
    }).catch(function(e){
    callback({'error':true, 'response': e});
});

};

return service;

});
