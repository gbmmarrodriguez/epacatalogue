'use strict';

angular.module('epaCatalogueInit')
.service('menuService', function(httpService, popUpService,nlcServices,UserService,translateService,shopService){
var th = this;

th.getMenuAreas = function(cb){
    httpService.allHttp('GET','','areas?filter[include]=subareas','',null,function(response){
    if (response.error) {
        popUpService.showPopup('Error', 'Lo sentimos, no se han podido obtener las areas');
    }
    else{
        cb(th.areasList = response.response.data);
      }
});
}

th.refreshSubAreas = function(name,id,cb){
    console.log(id,name);
    th.nameArea1= name;
    th.id= id;
    th.linkArea='/'+th.id+'/subareas';
    httpService.allHttp('GET','','areas',th.linkArea, null,function(response){
        if (response.error) {
            //console.error(response);
            popUpService.showPopup('Lo sentimos', 'No se pueden obtener las subareas en este momento, por favor intente mas tarde');
            th.ready = true;
        }
        else{
            th.subareasList = response.response.data;
        
            cb(th.subareasList);
        }
});
}
th.refreshProducts = function(id,name,cb){
    th.nameSubarea1 =name;
    th.id = id;
    th.limit = 10;
    th.skip = 0;
    th.linkSubarea='/'+th.id+'/products?filter[skip]='+th.skip.toString()+'&filter[limit]='+th.limit.toString();

    httpService.allHttp('GET', '', 'subareas', th.linkSubarea, null, function(response) {
      if (response.error) {
        popUpService.showPopup('Lo sentimos', 'No se puede obtener la lista de productos en este momento, por favor intente mas tarde');
      }
      else {
        th.productList = response.response.data;
        cb(th.productList);
        console.log(th.productList);

      }
    });


}

});
