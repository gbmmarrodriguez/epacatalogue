'use strict';

angular.module('epaCatalogueInit')
.service('shopService', function(httpService, popUpService,  UserService){
    var th = this;
    th.favoriteList = [];

    th.cartList = [];
    th.viewCombo = 'true';

    var isAndroid = window.ionic.Platform.isAndroid();
    var isIOS = window.ionic.Platform.isIOS();
    th.isPhone = isAndroid || isIOS;
    th.isPhone  = th.isPhone;

  th.addToFavorites = function(id, product){
    th.existFavorites(product, function(exist){
      if (exist) {
      popUpService.showPopup('Error', 'Este produto ya existe en la lista de favoritos');
    }else {
      th.favoriteList.push(product);
      var object = {
          userId : id,
          products : th.favoriteList
      };

      httpService.allHttp('POST', 'favorites','/upsertWithWhere?where[userId]='+id,'', object,function(response){
      if (response.error) {
          popUpService.showPopup('Error', 'No se ha podido actualizar el producto en la lista de favoritos');
      }
      else{
    //    console.log(response);
        popUpService.showPopup('Éxito', 'Producto agregado satisfactoriamente a favoritos');
      }
    });

    }
    });
    };

  th.addToCart = function(id, product, quantity){
    existCart(product, function(exist){
      if (exist) {
        popUpService.showPopup('Error', 'Este produto ya existe en el carrito de compra');

    }else {

          var productObject = {
            product : product,
            cartquantity : quantity
          };
          th.cartList.push(productObject);
          var arrayObject = {
              productCart : th.cartList,
              userId: id
          };

          httpService.allHttp('POST', 'carts','/upsertWithWhere?where[userId]='+id,'', arrayObject,function(response){
        //    console.log(response);
            if (response.error) {
                popUpService.showPopup('Error', 'No se ha podido actualizar el producto en el carrito de compra');

            }
            else{
              popUpService.showPopup('Éxito', 'Producto actualizado en el carrito de compra');
            }
          });

      }
      });
    };

  th.removeFromCart = function(id, index){
    var arrayBackup = th.cartList.slice();
    th.cartList.splice(index, 1);
    if(th.cartList.length == 0){
      var arrayObject = {
          productCart : th.cartList,
          userId: id
      };
    }else {
      var arrayObject = {
          productCart : th.cartList,
          userId: id
      };
    }

    httpService.allHttp('POST', 'carts','/upsertWithWhere?where[userId]='+id,'', arrayObject,function(response){
    if (response.error) {
          th.cartList = arrayBackup;
          popUpService.showPopup('Error', 'Lo sentimos, ha ocurrido un error al eliminar el producto');
    }
    else{
    //  console.log(response);
      popUpService.showPopup('Éxito', 'Producto eliminado satisfactoriamente del carrito');
    }
    });
  };

  th.removeFromFavorites = function(id, index){
    var arrayBackup = th.favoriteList.slice();
    th.favoriteList.splice(index, 1);
    if(th.favoriteList.length == 0){
      var object = {
          userId : id,
          products : th.favoriteList
      };
    }else {
      var object = {
          userId : id,
          products : th.favoriteList
      };
    }

      httpService.allHttp('POST', 'favorites','/upsertWithWhere?where[userId]='+id,'', object,function(response){
      if (response.error) {
        //console.log(response);
        th.favoriteList = arrayBackup;
        popUpService.showPopup('Error', 'Lo sentimos, ha ocurrido un error al eliminar el producto');
          //      console.log(th.favoriteList);
      }
      else{
          //      console.log(response);
        popUpService.showPopup('Éxito', 'Producto eliminado satisfactoriamente');
      }
      });

    };

  th.updateQuantity = function(id, cartList){
    th.cartList = cartList;
    var arrayObject = {
        productCart : th.cartList,
        userId: id
    };
    httpService.allHttp('POST', 'carts','/upsertWithWhere?where[userId]='+id,'', arrayObject,function(response){
    if (response.error) {
      popUpService.showPopup('Error', 'Lo sentimos, ha ocurrido un error al actualizar la cantidad');

    }
    else{
      //console.log(response);
      //popUpService.showPopup('Éxito', 'Cantidad actualizada');
    }
    });
  };


  th.reloadCarts = function(callback){
      if(UserService.actualUser == undefined){
          callback(1);
    }else {
      httpService.allHttp('GET', 'users','/'+UserService.actualUser.id,'/carts', null,function(response){
        if (response.error) {
            callback(2);
        }
        else{
          if (response.response.data[0] == undefined) {
            callback([]);
          }else {
            th.cartList = response.response.data[0].productCart;
            callback(response.response.data[0].productCart);
          }
        }
      });
    }};

  th.reloadFavorites = function(callback){
    if(UserService.actualUser == undefined){
        callback(1);
    }else {
      httpService.allHttp('GET', 'users','/'+UserService.actualUser.id,'/favorites', null,function(response){
      if (response.error) {
          callback(2);
      }
      else{
        if (response.response.data[0] == undefined) {
          callback([]);
        }else {
          th.favoriteList = response.response.data[0].products;
          callback(response.response.data[0].products);
        }
      }
    });
  }};

  th.existFavorites = function(product, callback){
    th.reloadFavorites(function(array){
      if (array.length == 0) {
        callback(false);
        return;
      }else {
        for (var i = 0; i < array.length; i++) {
          if(array[i].id == product.id){
            callback(true);
            return;
          }
          if ((i+1) == array.length) {
            callback(false);
            return;
          }
        }
      }
    });
  };

  var existCart = function(product, callback){
    th.reloadCarts(function(array){
      if (array.length == 0) {
        callback(false);
        return;
      }else {
        for (var i = 0; i < array.length; i++) {
          if(array[i].product.id == product.id){
            callback(true);
            return;
          }
          if ((i+1) == array.length) {
            callback(false);
            return;
          }
        }
      }
    });
  };

});
