'use strict';

angular.module('epaCatalogueInit')
  .factory('httpService',function($http){
  var service = {};

  var urlBase = 'https://api.us.apiconnect.ibmcloud.com/edwinirahetagmailcom-practicantes/epaservicesv3/api/';
  var x_ibm_client_id = '4a283666-d2e1-4a39-93c9-01ad6c428331';
  var x_ibm_client_secret = 'qQ6mI1pP3sC1jH6sM2fI0jE8tT5uN2bT5oE0yU8hF4tR4iU2qW';

  service.allHttp = function (httpMethod, module, methodApi, param, json, callback){
    var req = {
      async: true,
      crossDomain: true,
      method: httpMethod,
      headers: {
        'x-ibm-client-id': x_ibm_client_id,
        'x-ibm-client-secret': x_ibm_client_secret,
        'Content-Type' : 'application/json'
      }
    };

    req.url = urlBase + module +methodApi+param;
    //  console.log(req.url);

    if(json !== null){
      req.data = json;
    }

    return $http(req).then(function (response) {
      callback({'error':false, 'response': response}); //No hubo error
    }).catch(function(e){
      callback({'error':true, 'response': e}); //No hubo error
    });

  };

  return service;
});
