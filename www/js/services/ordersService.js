'use strict';

angular.module('epaCatalogueInit')
.factory("ordersService",function($http, $cordovaNetwork, $rootScope, $ionicPopup, $cordovaToast, $cordovaVibration){
    var service = {};

    //Url para el API Connect
    var url = 'https://api.us.apiconnect.ibmcloud.com/edwinirahetagmailcom-practicantes/epa-services/api/';

    //Encabezados para el API Connect
    var headersBase =
    {
      'X-IBM-Client-Id': '41595e44-1927-434a-9759-7d15ccdcf353',
      'X-IBM-Client-Secret': 'xR1iJ1cA7dR7fV3xC3hJ4oB6iM3fQ0mK3mV3nI6uT4nH7vS7uY',
      'content-type': 'application/json',
      'accept': 'application/json'
    };

    //-----------------------Llamada GET al API ----------------------------
    service.GETApi = function(module){
      var req = {
        method: 'GET',
        url: url + module,
        headers: headersBase
      };
      return $http(req).then(function(response){
        return response.data;
      }).catch (function(err){
        if($cordovaNetwork.isOnline()){
          var alertPopup = $ionicPopup.alert({
            title: 'Error en el servidor',
            content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
          });
        }
        else{
            var alertPopup = $ionicPopup.alert({
              title: 'Sin acceso a Internet',
              content: 'No hay conexión a internet. Por favor habilitar la conexión e intentar de nuevo.'
            });
        }
      });
    };

    //-----------------------Llamada POST al API ----------------------------
    service.POSTApi = function (module, jsonFile){
      var req = {
        method: 'POST',
        url: url + module,
        headers: headersBase,
        data: jsonFile
      }

      return $http(req).then(function (response) {
         return true;
      }).catch (function(err){
          if($cordovaNetwork.isOnline()){
            var alertPopup = $ionicPopup.alert({
              title: 'Error en el servidor',
              content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
            });
          }
      });
    };

     //-----------------------Llamada POST con Response al API ----------------------------
    service.POSTApiV2 = function (module, jsonFile){
      var req = {
        method: 'POST',
        url: url + module,
        headers: headersBase,
        data: jsonFile
      }

      return $http(req).then(function (response) {
         return response.data;
      }).catch (function(err){
          if($cordovaNetwork.isOnline()){
            var alertPopup = $ionicPopup.alert({
              title: 'Error',
              content: err.data.error.message
            });
          }
      });
    };

//-------------Llamada POST al API para agregar una comanda---------------------
    service.POSTAddOrder = function(clientId, headquarterId, userID){
      var req = {
        method: 'POST',
        url: url + 'orders/addOrder?client='+ clientId +'&headquarter='+ headquarterId + '&user_id=' + userID,
        headers: headersBase
      };
    //  console.log(req);
      return $http(req).then(function (response) {
        return response.data;
      }).catch (function(err){
          /*if($cordovaNetwork.isOnline()){
            var alertPopup = $ionicPopup.alert({
              title: 'Error en el servidor',
              content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
            });
          }*/
      });
    };

//---------------Llamada POST al API para agregar un producto a una comanda-----------------
    service.POSTAddProductoTOOrder = function(order_id, work_area_id, userId, productsArray){
     // console.log("JSON FILE: ", productsArray);
      var req = {
        method: 'POST',
        url: url + 'product_orders/addItem?order=' + order_id + '&work_area=' + work_area_id + '&user_id=' + userId,
        headers: headersBase,
        data: productsArray
      };
      return $http(req).then(function (response) {
        return response.data;
      }).catch (function(err){
          //console.log("Entre al error y el error q traigo es este: ", err);
          if(err.data.error.errno == 1452){
            //$cordovaVibration.vibrate(400);
            //$cordovaToast.show('EL producto no existe en el catálogo de productos', 'short', 'center');
        //    console.log('EL producto no existe en el catálogo de productos');
          }else{
            //console.log("Err", err);
          }
          return err;
      });
    };

    //-----------------------Llamada PUT al API ----------------------------
    service.PUTApi = function(module, id, jsonFile){
      var req = {
         method: 'PUT',
         url: url + module + '/'+ id,
         headers: headersBase,
         data: jsonFile
      }

      return $http(req).then(function (response) {
         return response.data;
      }).catch(function(err){
          if($cordovaNetwork.isOnline()){
            var alertPopup = $ionicPopup.alert({
              title: 'Error el en servidor',
              content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
            });
          }
          return false;
      });
    };

    //-----------------------Llamada PUT sin ID y con filtro al API ----------------------------
    service.PUTFilterApi = function(module, jsonFile){
      var req = {
         method: 'PUT',
         url: 'https://api.us.apiconnect.ibmcloud.com/edwinirahetagmailcom-practicantes/epa-services/api/' + module,
         headers: {
              'X-IBM-Client-Id': '41595e44-1927-434a-9759-7d15ccdcf353',
              'X-IBM-Client-Secret': 'xR1iJ1cA7dR7fV3xC3hJ4oB6iM3fQ0mK3mV3nI6uT4nH7vS7uY',
              'content-type': 'application/json',
              'accept': 'application/json'
            },
         data: jsonFile
      }

      return $http(req).then(function (response) {
         return response.data;
      }).catch(function(err){
          return err
      });
    };


    //------------------Llamada PUT al API  para actualizar el detalle de la comanda------------------
    service.PUTEditDetailFromOrder = function(module, jsonFile, userID){
     // console.log("El JSON File: ", jsonFile);
      var req = {
         method: 'PUT',
         url: url + module + '/updateProductOrder?user_id=' + userID,
         headers: headersBase,
         data: jsonFile
      }

      return $http(req).then(function (response) {
         return response.data;
      }).catch(function(err){
          if($cordovaNetwork.isOnline()){
            var alertPopup = $ionicPopup.alert({
              title: 'Error el en servidor',
              content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
            });
          }
          return false;
      });
    };

    //-----------------------Llamada DELETE al API ----------------------------
    service.DELApi = function(module, id){
      var req = {
        method: 'DELETE',
        url: url + module + '/'+ id,
        headers: headersBase
      };

      return $http(req).then(function(response){
         return response.data;
      }).catch (function(err){
        if($cordovaNetwork.isOnline()){
          var alertPopup = $ionicPopup.alert({
            title: 'Error el en servidor',
            content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
          });
        }
        return false;
      });
    };

    //---------------Llamada DELETE detalle de una comanda ---------------------
    service.DELDetailFromOrder = function(module, idOrder, idPrduct, userId){
      var req = {
        method: 'DELETE',
        url: url + module + '/productOrderDelete?productID=' + idPrduct + '&orderID=' + idOrder + '&user_id=' + userId,
        headers: headersBase
      };

      return $http(req).then(function(response){
         return response.data;
      }).catch (function(err){
        if($cordovaNetwork.isOnline()){
          var alertPopup = $ionicPopup.alert({
            title: 'Error el en servidor',
            content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
          });
        }
        return false;
      });
    };

    //-------------Llamada DELETE sin id y con filtro al API --------------------
    service.DELFilterApi = function(module){
      //console.log("Soy la URL pa borrash: ", url + module);
      var req = {
        method: 'DELETE',
        url: url + module,
        headers: headersBase
      };

      return $http(req).then(function(response){
    //    console.log("Soy el response: ", response)
        return response.data;
      }).catch (function(err){
    //    console.log("Soy el err: ", err)
        if($cordovaNetwork.isOnline()){
          var alertPopup = $ionicPopup.alert({
            title: 'Error el en servidor',
            content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
          });
        }
        return false;
      });
    };

    //--------Detectar cuando el dispositivo tiene acceso a internet --------------
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      $cordovaToast.show('Acceso a internet habilitado', 'short', 'center');
    });

    //--------Detectar cuando el dispositivo no tiene acceso a internet --------------
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      var alertPopup = $ionicPopup.alert({
        title: 'Sin acceso a Internet',
        content: 'No hay conexión a internet. Por favor habilitar la conexión e intentar de nuevo.'
      });
    });

    return service;
});
