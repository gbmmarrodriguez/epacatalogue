'use strict';

 angular.module('epaCatalogueInit')
 .service('popUpService',function($ionicPopup,$state){
   var th=this;
   th.showPopupGo = function(pTitle,pMessage,state) {
      var alertPopup = $ionicPopup.alert({
        title: pTitle,
        template: pMessage
      }).then(function(res) {
        //console.log(state);
           $state.go(state);
      });
   };

   th.showPopup = function(pTitle,pMessage) {
      var alertPopup = $ionicPopup.alert({
        title: pTitle,
        template: pMessage
      });
   };

   th.askHeadquarterClient = function(scope, callback) {
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '<input id="headquarter" type = "text" value="Demo_Headquarter_1">',
         title: 'Realizar pedido',
         subTitle: 'Favor ingresar la sede donde desear recibir su pedido',
         scope: scope,

         buttons: [
            { text: 'Cancelar' }, {
               text: '<b>Aceptar</b>',
               type: 'button-positive',
                  onTap: function(e) {

                    var headquarter = document.getElementById('headquarter').value;
                     if (!headquarter) {
                        //don't allow the user to close unless he enters model...
                           e.preventDefault();
                     } else {
                        return headquarter;
                     }
                  }
            }
         ]
      });

      myPopup.then(function(res) {
        callback(res);
      });
   };

 });
