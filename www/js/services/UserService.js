'use strict';

angular.module('epaCatalogueInit')
.service('UserService', function($cookies){
  var th = this;
  th.isAuth = false;
  th.actualUser =  $cookies.getObject('usr');

});
