﻿'use strict';

angular.module('epaCatalogueInit')
.factory('mailService',function($http){
  var service = {};

  var urlBase = 'https://epamailservicecors.mybluemix.net/';

  service.sendMail = function (httpMethod, module, methodApi, param, json, callback){
    var req = {
      async: true,
      crossDomain: true,
      method: httpMethod,
      headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
      }
    };

    req.url = urlBase + module +methodApi+param;
    //console.log(req.url);

    if(json !== null){
      req.data = json;
    }

 return $http(req).then(function (response) {
           callback({'error':false, 'response': response}); //No hubo error
      }).catch(function(e){
        callback({'error':true, 'response': e}); //No hubo error
      });

};

service.sendBillMail = function (httpMethod, module, methodApi, param,carProducts,email, callback){
  var req = {
    async: true,
    crossDomain: true,
    method: httpMethod,
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
    }
  };

  req.url = urlBase + module +methodApi+param;
  //console.log(req.url);


    var texto='<img src="https://res.cloudinary.com/dqswkssio/image/upload/'+
    't_media_lib_thumb/v1493925299/epa-logo_ifqk8q.png" alt="EPA Logo"/>';
    texto+="<p>¡En EPA es un gusto servirle! A continuación, el <br></br><p/>";
    texto+="<p>informe de su de Comanda:</p>";
    texto+="<!DOCTYPE html><html><head><style>";
    texto+="im {color: #ffffff}";
    texto+="td {text-align: center;}";
    texto+="tr:hover {background-color: #f5f5f5}";
    texto+="th {background-color: #92a8d1}";
    texto+="</style></head><body>";
    texto+="<table>";
    texto+="<tr>";
    texto+="<th>Nombre</th><th>Cantidad</th><th>Precio Unitario</th><th>Total por Producto</th>";
    texto+="</tr>";
    var total=0;
    var totalXProd=0;
    for (var i = 0; i < carProducts.length; i++) {
      //console.log(carProducts[i]);
      texto+='<tr>';
      texto+='<td>'+carProducts[i].product.name+'</td>';
      texto+='<td>  '+carProducts[i].cartquantity+'</td>';
      texto+='<td>  ₡'+carProducts[i].product.price+'</td>';
      totalXProd=parseFloat(carProducts[i].product.price)*parseFloat(carProducts[i].cartquantity);
      //console.log(totalXProd);
      total+=totalXProd;
      totalXProd=totalXProd.toFixed(4);
      var totalXProd_String=totalXProd.toString();
      texto+='<td>  ₡'+totalXProd+'</td>';
      texto+='</tr>';
    }
    total=total.toFixed(4);
    texto+='<br></br><br></br><tr></tr><tr></tr><tr></tr>';
    texto+='<td><b>Total:</b></td><td></td><td></td>';
    texto+='<td><b>  ₡'+total.toString()+'</b></td>';
    var objeto={
      "toEmail":email,
      "subject":"Creación de Comanda",
      "content":texto
    };
    req.data=angular.toJson(objeto);
    return $http(req).then(function (response) {
         callback({'error':false, 'response': response}); //No hubo error
    }).catch(function(e){
      callback({'error':true, 'response': e}); //hubo error
    });

};

return service;
});
