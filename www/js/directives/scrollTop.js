angular.module('epaCatalogueInit')

.directive('epaScroll', function($ionicScrollDelegate){
  return{
    templateUrl:"templates/scrollTop/scrollTop.html",
    link: function(scope, element, attrs) {
      element.on('click', function(e) {
          $ionicScrollDelegate.scrollTop(true);
      });
    }
  };

})

.directive('epaMovescroll', function($ionicScrollDelegate) {
return {
    restrict: 'A',
    scope: false,
    link: function(scope, element, attrs) {
        element.on("scroll", function(event) {
          var scrollPosition = $ionicScrollDelegate.getScrollPosition().top;
          //console.log(scrollPosition);
          var arrayScrolls = document.getElementsByClassName('floatScroll');
          if (scrollPosition > 20) {
              //document.getElementById("scrollTop").style.display = "block";
              for (i = 0; i < arrayScrolls.length; i++) {
                  arrayScrolls[i].style.display = 'block';
              }
          } else {
              //document.getElementById("scrollTop").style.display = "none";
              for (i = 0; i < arrayScrolls.length; i++) {
                  arrayScrolls[i].style.display = 'none';
              }
          }
        });
    }
  };
});
