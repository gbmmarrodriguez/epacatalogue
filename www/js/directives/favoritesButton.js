angular.module('epaCatalogueInit')
.directive('favoritesButton', function(UserService, popUpService, $state, shopService){
  return {
    restrict: 'AE',
    scope: {
      'productid' : '='
    },
    templateUrl: 'templates/favoritesButton/favoritesButton.html',
    link: function(scope, element, attrs) {
      scope.favoritesStyle = {
          'background-color':'#3c8dbc'
      };

      element.on('click', function(e) {
        //console.log(scope.productid);
        if(UserService.actualUser != undefined){
            shopService.addToFavorites(UserService.actualUser.id, shopService.actualProduct);
            updateFavoriteButton(scope.productid);
        }else {
            popUpService.showPopup('Iniciar sesión','Debe iniciar sesión para agregar productos a favoritos');
            UserService.pageToRequiredLogin = 'app.productDetail';
            $state.go('app.login');
        }
      });

      var updateFavoriteButton = function(id){
        var product = {'id':id};
        if (UserService.actualUser != undefined && shopService.favoriteList != undefined
          && shopService.actualProduct != undefined) {
          shopService.existFavorites(product, function(exist){
              if (exist) {
                scope.favoritesStyle['background-color'] = '#e62252';
              }else {
                scope.favoritesStyle['background-color'] = '#3c8dbc';
              }
          });
        };
      };



      scope.$watch(function () {
              return shopService.actualProduct;
          },
          function (value) {
            if (shopService.actualProduct != undefined) {
                updateFavoriteButton(shopService.actualProduct.id);
            }
          }
      );

      scope.$watch(function () {
              return UserService.actualUser;
          },
          function (value) {
            if (shopService.actualProduct != undefined) {
                updateFavoriteButton(shopService.actualProduct.id);
            }
          }
      );

      scope.$watch(function () {
              return shopService.favoriteList;
          },
          function (value) {
            if (shopService.actualProduct != undefined) {
                updateFavoriteButton(shopService.actualProduct.id);
            }
          }
      );

    }
  };
});
