angular.module('epaCatalogueInit')
        .directive('clickLogout', Clicky);

    function Clicky($state, UserService,$cookies) {
        return {
            restrict: 'A',
            scope: {
            },
            link: function(scope, element, attrs){
               element.on('click', function(e) {
                 console.log("Click");
                 if(!scope.$$phase) {
                     scope.$apply(function(){
                        UserService.actualUser = null;
                        $cookies.remove('usr');
                        $state.reload(true);
                        location.reload(true);
                     });
                 }else {
                    UserService.actualUser = null;
                    $cookies.remove('usr');
                    $state.reload(true);
                    location.reload(true);
                 }
               });
            }
      };
    };
