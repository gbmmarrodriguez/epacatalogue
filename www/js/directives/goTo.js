angular.module('epaCatalogueInit')
.directive('goTo', function($state, shopService, UserService, popUpService){
  return {
    restrict: 'A',
    scope: {
    },
    link: function(scope, element, attrs) {
      element.on('click', function(e) {
        if (attrs.goTo === 'app.favorites' || attrs.goTo === 'app.shopCart'
        || attrs.goTo === 'app.profile' || attrs.goTo === 'app.changePassword') {
          if (UserService.actualUser == undefined) {
            popUpService.showPopup('Inicio de sesión requerido',
            'Debe iniciar sesión para continuar');
            UserService.pageToRequiredLogin = attrs.goTo;
            $state.go('app.login');
          }else {
            $state.go(attrs.goTo);
          }
        }else if (attrs.goTo === 'app.promotion') {
          shopService.deal = true;
          $state.go(attrs.goTo);
        }
        else {
          $state.go(attrs.goTo);
        }

      });
    }
  };
});
