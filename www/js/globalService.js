angular.module('epaCatalogueInit')

//Global service: Posee funciones globales que utilizan los controladores
.factory("globalService",function($http, $cordovaNetwork, $rootScope, $ionicPopup, $cordovaToast, $cordovaVibration){
    var service = {};
    //Url para el API Connect
    var url = 'https://api.us.apiconnect.ibmcloud.com/edwinirahetagmailcom-practicantes/epav2/api/EPA/';

    //Encabezados para el API Connect
    var headersBase =
    {
      'X-IBM-Client-Id': '31d888f2-1b19-45e0-94cc-4481b45d1a7c',
      'X-IBM-Client-Secret': 'yU4fU4fL4lR4kS0eN6oW5rK3nD4yP8vA8fQ4xP5mH8tC7vX3vD',
      'content-type': 'application/json',
      'accept': 'application/json'
    };

    //-----------------------Llamada GET al API ----------------------------
    service.GETApi = function(module){
      var req = {
        method: 'GET',
        url: url + module,
        headers: headersBase
      };
      return $http(req).then(function(response){
        return response.data;
      }).catch (function(err){
        if($cordovaNetwork.isOnline()){
          var alertPopup = $ionicPopup.alert({
            title: 'Error en el servidor',
            content: 'Lo sentimos ha ocurrido un error en el servidor, favor intentar más tarde'
          });
        }
        else{
            var alertPopup = $ionicPopup.alert({
              title: 'Sin acceso a Internet',
              content: 'No hay conexión a internet. Por favor habilitar la conexión e intentar de nuevo.'
            });
        }
      });
    };

    //--------Detectar cuando el dispositivo tiene acceso a internet --------------
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      $cordovaToast.show('Acceso a internet habilitado', 'short', 'center');
    });

    //--------Detectar cuando el dispositivo no tiene acceso a internet --------------
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      var alertPopup = $ionicPopup.alert({
        title: 'Sin acceso a Internet',
        content: 'No hay conexión a internet. Por favor habilitar la conexión e intentar de nuevo.'
      });
    });

    return service;
});
