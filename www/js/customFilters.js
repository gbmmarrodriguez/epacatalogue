'use strict';

angular.module('epaCatalogueInit')
.filter('ampersand', function(){
    return function(input){
        return input ? input.replace(/&amp;/, '&') : '';
    }
})
.filter('thumb', function(){
  return function(input){
      return input ? input.replace('super_zoom', 'large_thumb'): '';
  }
});
